# proj2-flask
A starter project for using the Flask framework

Author: Emmalie Dion
Path on ix: home/users/emmalie/proj2-flask
BitBucket URL: https://bitbucket.org/EmDion14/proj2-flask
ix url: http://ix.cs.uoregon.edu:7557

##Assignment

This program will bring up a class syllabus in a web browser. Based on the current date the program will highlight the current week, so that students know which assignments to focus on. The program will also tell the students which week of the term it is and the date of the first day of the week. 
